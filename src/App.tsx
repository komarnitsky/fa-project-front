import React from 'react';
import './App.css';
import {ProfilePage} from './profile/profilePage';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import {CustomProjectPage} from './profile/Content/CustomProjectPage';

const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/">
          <ProfilePage activeTab="analytics" />
        </Route>
        <Route path="/analytics">
          <ProfilePage activeTab="analytics" />
        </Route>
        <Route path="/work">
          <ProfilePage activeTab="work" />
        </Route>
        <Route path="/study">
          <ProfilePage activeTab="study" />
        </Route>
        <Route path="/:project">
          <CustomProjectPage />
        </Route>
      </Switch>
    </BrowserRouter>
  );
};

export default App;
