import React, {FC} from 'react';
import {Col, Row, Select, Typography} from 'antd';
import {HeatmapFilterProps} from '../types';

const {Text} = Typography;
const {Option} = Select;

const getYears = () => {
  const arr = [];
  for (let i = 2017; i <= new Date().getFullYear(); i++) {
    arr.push(i);
  }
  return arr;
};

export const HeatmapFilter: FC<HeatmapFilterProps> = (
    {
      filters,
      setFilters,
    }) => {
  const getProjectList = () => {
    // TODO: запрос на бэк
    return ['work', 'personal', 'study', 'custom', 'any']
        .map((e) => ({name: e, value: e}));
  };

  const updateFilters = (update: {year?: number; project?: string}) => {
    setFilters({
      ...filters,
      ...update,
    });
  };

  return (
    <div className="filterContainer">
      <Row>
        <Col span={4}>
          <Text>Год:{' '}</Text>
        </Col>
        <Select
          placeholder="Год"
          defaultValue={new Date().getFullYear()}
          className="filterSelect"
          onSelect={(value) => updateFilters({year: value})}
        >
          {getYears().map((e) =>
            (<Option value={e} key={e}>{e}</Option>),
          )}
        </Select>
      </Row>
      <br/>
      <Row>
        <Col span={4}>
          <Text>Проект:{' '}</Text>
        </Col>
        <Select
          placeholder="Проект"
          defaultValue={'any'}
          className="filterSelect"
          onSelect={(value) => updateFilters({project: value})}
        >
          {getProjectList().map((e) => (
            <Option value={e.value} key={e.value}>{e.name}</Option>
          ))}
        </Select>
      </Row>
    </div>
  );
};
