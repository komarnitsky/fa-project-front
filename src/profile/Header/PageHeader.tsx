import React, {FC} from 'react';
import {Button, Layout, Typography} from 'antd';
import './PageHeader.css';
import {HeaderProps} from './types';

const {Header} = Layout;
const {Title} = Typography;

export const PageHeader: FC<HeaderProps> = ({username}) => {
  return (
    <Header className="header">
      <Title level={4} style={{color: 'white', float: 'left'}}>
            Личный кабинет {username}
      </Title>
      <Button
        ghost={true}
        style={{float: 'right'}}
      >
        Выход
      </Button>
    </Header>
  );
};
