import React, {FC} from 'react';
import {ProjectProps} from './types';
import {Typography} from 'antd';

export const ProjectPage: FC<ProjectProps> = ({project}) => {
  return (
    <div style={{alignItems: 'center', justifyContent: 'center'}}>
      <Typography.Text>Привет! Я {project}</Typography.Text>
    </div>
  );
};
