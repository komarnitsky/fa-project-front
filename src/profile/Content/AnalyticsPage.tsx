import React from 'react';
import {ChartContainer} from '../../analytics/ChartContainer/ChartContainer';
import {Card, Typography} from 'antd';
import './Content.css';

const {Title} = Typography;

export const AnalyticsPage = () => {
  return (
    <Card className="pageContainer">
      <Title>Аналитика</Title>
      <ChartContainer chartType="heatmap" />
    </Card>
  );
};
