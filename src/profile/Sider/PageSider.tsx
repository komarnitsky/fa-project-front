import React, {FC, useState} from 'react';
import {Layout, Menu} from 'antd';
import './PageSider.css';
import {Link} from 'react-router-dom';
import {
  PieChartOutlined,
  UserOutlined,
  DesktopOutlined,
  BookOutlined,
  PlusOutlined,
} from '@ant-design/icons';
import {SiderProps} from './types';

const {Sider} = Layout;

export const PageSider: FC<SiderProps> = ({collapsed=false, activeTab}) => {
  const [isCollapsed, setCollapsed] = useState(collapsed);

  const onSiderCollapse = () => {
    setCollapsed(!isCollapsed);
    // TODO: отправить на сервер
  };

  return (
    <Sider collapsible collapsed={isCollapsed} onCollapse={onSiderCollapse}>
      <div className="logo" />
      <Menu theme="dark" defaultSelectedKeys={[activeTab]} mode="inline">
        <Menu.Item key="analytics" icon={<PieChartOutlined />}>
          <Link to="/analytics">Аналитика</Link>
        </Menu.Item>
        <Menu.Item key="personal" icon={<UserOutlined />}>
          <Link to="/personal">Личное</Link>
        </Menu.Item>
        <Menu.Item key="work" icon={<DesktopOutlined />}>
          <Link to="/work">Работа</Link>
        </Menu.Item>
        <Menu.Item key="study" icon={<BookOutlined />}>
          <Link to="/study">Учеба</Link>
        </Menu.Item>
        {/* TODO: custom projects */}
        <Menu.Item key="createNewProject" icon={<PlusOutlined />}>
          <a>Новый проект</a>
        </Menu.Item>
      </Menu>
    </Sider>
  );
};
